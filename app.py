from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/credentials')
def credentials():
    author_info = "Autor: Bartłomiej Pryczek"
    return render_template('credentials.html', author=author_info)

if __name__ == '__main__':
    app.run(debug=True)
